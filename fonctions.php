<!--FONCTIONS-->
<?php
/*******************************
 ******** LES FONCTIONS ********
 *******************************/

// function Bonjour()
// {
//     echo "Bonjour à tous!<br>";
// }

// Bonjour();


// function BonjourUtilisateur($prenom)
// {
//     echo 'Bonjour ' . $prenom . '!<br>';
// }

// BonjourUtilisateur('Marc'); //Bonjour Marc!


// function NomAge($prenom, $age)
// {
//     echo $prenom . ' a ' . $age . ' ans<br>';
// }

// NomAge('Marc', '41');


// /*******************************
//  ******* return VS echo ********
//  *******************************/


// function DireBonjour()
// {
//     return "Bonjour";
// }

// function DireBonsoir()
// {
//     echo "Bonsoir";
// }
// $bonjour = DireBonjour();
// $bonsoir = DireBonsoir();

// echo $bonjour;
// echo $bonsoir . '<br>';


/*******************************
 **** CHAÎNES DE CARACTÈRE *****
 *******************************/

echo strlen("Bonjour à tous") . '<br>'; //15
echo str_word_count("Bonjour à tous") . '<br>'; //2
echo str_word_count("Bonjour à tous", 0, 'à') . '<br>'; //3
print_r(str_word_count("Bonjour à tous", 1));
print_r(str_word_count("Bonjour à tous", 2));
echo str_word_count("Salut l'ami") . '<br>'; //2 (mettre un espace après le l' pour qu'il soit compté comme un mot)
echo str_word_count("Bonjour a tous") . '<br>'; //3


$str = "Salut l'ami, vous avez une b3lle mine !";
$str2 = "Je suis content";

//COMPTER NOMBRE DE MOTS
print_r(str_word_count($str, 2));
print_r(str_word_count($str2, 2));

echo str_repeat("Bonjour \n", 7);

//REMPLACER UN MOT
$texte = "<br> Bonjour, comment allez-vous ?";
echo str_replace("Bonjour", "Bonsoir", $texte, $i); /*On remplace Bonjour par Bonsoir danas la variable $texte.
Le 4e paramètre est une varaible dans laquelle on stocke le nombre de remplacements*/
//Fonction sensible à la casse / stt_ireplace est insensible à la casse
echo "<br> Voici le nombre de remplacements: $i <br>";

//CASSE
$minmaj = "Bonjour, VouS aLLez Bien ?";
echo strtolower($minmaj) . '<br>'; //strtoupper() pour majuscule
echo ucfirst(strtolower($minmaj)); //sauf la première lettre

echo strpos("Bonjour", "o") . '<br>'; // 1
echo strpos("Bonjour à tous", "tous") . '<br>'; // 11
echo strpos("Bonjour", "e") . '<br>'; // rien


//AUTRES FONCTIONS
/*
< => &lt
> => &gt
& => &amp
" => &quot
' => $#039
*/
$str3 = 'J\'aime le <strong>"PHP"</strong>';

echo htmlspecialchars($str3);
echo htmlspecialchars_decode($str3);

echo "<br><br>";
//nl2br (NO LINE TO BREAK)
echo nl2br("Bonjour !
Comment ça va ?");

echo "<br><br>";

//FOR et COUNT

$utilisateurs = [
    ["nom" => "Waganwheel", "prenom" => "Jam", "comment" => "c'est hat les tableaux"],
    ["nom" => "Wagaewheel", "prenom" => "Jem", "comment" => "c'est het les tableaux"],
    ["nom" => "Waginwheel", "prenom" => "Jim", "comment" => "c'est hit les tableaux"],
    ["nom" => "Wagonwheel", "prenom" => "Jom", "comment" => "c'est hot les tableaux"],
    ["nom" => "Wagunwheel", "prenom" => "Jum", "comment" => "c'est hut les tableaux"],
    ["nom" => "Wagynwheel", "prenom" => "Jym", "comment" => "c'est hyt les tableaux"]
];
//FOR
for ($i = 0; $i <= 10; $i++) {
    print_r($i);                //012345678910
}
echo "<br>";

//COUNT
print_r(count($utilisateurs));         //6
echo "<br>";

// for ($i = 0; $i <= count($utilisateurs); $i++) {
//     echo $utilisateurs[$i]["prenom"] . " ";     //Jam Jem Jim Jom Jum Jym
// }


//SÉPARER DES CHAÎNES DE CARACTÈRE
$ch2tb = "Bonjour à tous";
print_r(explode(" ", $ch2tb)); // Array ( [0] => Bonjour [1] => à [2] => tous )

echo "<br><br>";

print_r(str_split("Bonjour", 2)); // Array ( [0] => Bo [1] => nj [2] => ou [3] => r )

echo "<br><br>";

//JOINDRE DES CHAÎNES DE CARACTÈRE

$tb2ch = array('Bonjour', 'à', 'tous');
echo implode(" ", $tb2ch); // Bonjour à tous

echo "<br><br>";

//FONCTIONS TABLEAUX

//array_keys
$voitures = array(
    "Citroen" => "DS3",
    "Renault" => "Clio",
    "Peugeot" => "306",
    // "Peugeot2" => 306
);

print_r(array_keys($voitures)); //Array ( [0] => Citroen [1] => Renault [2] => Peugeot [3] => Peugeot2 )
echo "<br><br>";
print_r(array_keys($voitures, 306, true)); //Array ( [0] => Peugeot2 ) ---> TRUE teste aussi le type de variable
echo "<br><br>";
print_r(array_keys($voitures, 306, false)); //Array ( [0] => Peugeot [1] => Peugeot2 ) ---> FALSE et la valeur par défaut, pas nécessaire
echo "<br><br>";

//CHERCHER UNE CLEF DANS UN TABLEAU (sensible à la casse)
if (array_key_exists("Citroen", $voitures)) {
    echo "La clef existe";
} else {
    echo "La clef n'existe pas";
}
echo "<br><br>";

//CHERCHER UNE VALEUR DANS UN TABLEAU ET RENVOIE LA CLEF (sensible à la casse)
echo array_search("DS3", $voitures); //Citroen
echo "<br><br>";

//On peut ajouter un 3e paramètre pour vérifier si le type de valeurs est le même
//Donc, il renvoie la clef seulement si, en plus d'être présente, la valeur est du même type de variable
//Si on n'écrit rien ou false, il retournera la clef même si la valeur de la variable est de type différent
echo array_search(306, $voitures, true);


$prenoms = ["Pierre", "Paul", "Jacques"];

//in_array est sensible à la casse, mais pas au type de valeur. On peut ajouter TRUE pour spécifier qu'on veut le même type de valeur
if (in_array("Pierre", $prenoms)) {
    echo "Ce prénom fait bien partie du tableau";
} else {
    echo "Ce prénom ne fait pas partie du tableau";
};
echo "<br><br>";

//array_values()
$loisirs = [
    "sport" => "Tennis",
    "voyage" => "Écosse",
    "musique" => "Punk"
];
echo "<br><br>";

echo "<pre>";
print_r(array_values($loisirs)); // Crée un nouveau tableau avec valeurs seulement
echo "</pre>";

//COUNT, ARRAY_COUNT
$sports = ["hockey", "tennis", "hockey", "baseball"];
echo count($sports);
echo "<br><br>";

echo "<pre>";
print_r(array_count_values($sports));
echo "</pre>";

//array_diff_assoc compare 2 tableaux ou plus et renvoie clés et valeurs dont les clés et les valeurs du 1er tableau qui NE SONT présentes dans AUCUN des tableaux qui suivent
//sensible à la casse sur valeurs et clés

//array_diff_key compare les clés et renvoie clés et valeurs dont les CLÉS NE SONT présentes dans AUCUN des tableaux qui suivent
//array_diff compare les valeurs et renvoie clés et valeurs dont les VALEURS NE SONT présentes dans AUCUN des tableaux qui suivent

//array_intersect_assoc compare 2 tableaux ou plus et renvoie clés et valeurs dont les clés et les valeurs du 1er tableau SONT présentes dans tous les tableaux qui suivent

$tab1 = [
    "sport" => "hockey", "couleur" => "bleu", "musique" => "hip hop", "ethnie" => "caucasien", "ville" => "Québec"
];

$tab2 = [
    // "sport" => "hockey", "couleur" => "vert", "musique" => "punk", "groupe" => "Pearl Jam", "ethnie" => "caucasien"
    "sport" => "course à pied", "couleur" => "rouge", "musique" => "punk", "ethnie" => "noir", "ville" => "Québec"
];

$tab3 = [
    "sport" => "tennis", "couleur" => "bleu", "musique" => "classique", "ethnie" => "asiatique", "ville" => "Québec"
];

print_r(array_diff_assoc($tab1, $tab2, $tab3));
echo "<br><br>";
print_r(array_diff_assoc($tab2, $tab1, $tab3));
echo "<br><br>";
print_r(array_intersect_assoc($tab1, $tab2, $tab3));
echo "<br><br>";
print_r(array_intersect_assoc($tab1, $tab2, $tab3));
echo "<br><br>";


//AJOUTER ET EFFACER DES DONNÉES DANS UN TABLEAU
$remplissage = array_fill(0, 5, "vert");

print_r(array_fill(3, 4, "rouge")); //On ajouter 4 fois la valeur "rouge" à partir de l'index 3
echo "<br><br>";
print_r($remplissage);
echo "<br><br>";

$clefs = ["a", "b", "c", "d"];
$remplir = array_fill_keys($clefs, "bleu"); //Array ( [a] => bleu [b] => bleu [c] => bleu [d] => bleu )

print_r($remplir);
echo "<br><br>";

$couleur = ["bleu", "vert"];

array_push($couleur, "rouge", "jaune");
print_r($couleur);
echo "<br><br>";

array_pop($couleur); //SUPPRIME le dernier élément du tableau
print_r($couleur); //affiche le nouveau tableau
echo "<br><br>";
print_r(array_pop($couleur)); //SUPPRIME et affiche l'élément de la fin qui a été supprimé 
echo "<br><br>";
print_r($couleur); //affiche le nouveau tableau sans le dernier élément que l'on vient de supprimer et afficher
echo "<br><br>";

array_unshift($couleur, "bleu", "violet"); //AJOUTE les éléments au début d'un tableau
print_r($couleur);
echo "<br><br>";

array_shift($couleur); //SUPPRIME le premier élément du tableau
print_r($couleur);
echo "<br><br>";

$couleur1 = ["a" => "bleu", "b" => "vert", "c" => "jaune", "d" => "orange", "e" => "blanc"];
$couleur2 = ["a" => "rouge", "b" => "violet", "c" => "rose", "d" => "gris", "e" => "noir"];

array_splice($couleur1, 2); //supprime toutes les valeurs à partir de la position 2
print_r($couleur1); // Array ( [a] => bleu [b] => vert )
echo "<br><br>";
print_r(array_splice($couleur2, 1)); //affiche un tableau contenant les éléments supprimés
echo "<br><br>"; // Array ( [b] => violet [c] => rose [d] => gris [e] => noir )
array_splice($couleur1, 0, 1, $couleur2); //supprime 1 élément de $couleur1 à partir de l'index 0 et les remplace par les valeurs de la de $couleur2
print_r($couleur1); // Array ( [0] => rouge [b] => vert )
echo "<br><br>";


//TRIER ET COMBINER DES TABLEAUX
$alpha = ["a", "b"];
$beta = ["c", "d"];

$omega = array_merge($alpha, $beta);
print_r($omega); //Array ( [0] => a [1] => b [2] => c [3] => d )

$alpha1 = ["a", "b"];
$beta1 = ["c", "d"];

// array_combine($alpha1, $beta1)